<?php
/**
 * Basis
 *
 * @author James Clébio <jamesclebio@gmail.com>
 * @link https://github.com/jamesclebio/basis 
 * @license http://jamesclebio.mit-license.org/
 */

namespace Basis\Base;

require_once 'Setup.php';
require_once 'Autoload.php';

set_include_path(INCLUDES);

spl_autoload_register('Autoload::core');
spl_autoload_register('Autoload::helper');
spl_autoload_register('Autoload::model');

