<section class="section-content" id="home">
	<div class="billboard">
		<div class="container">
			<div class="item">
				<div class="image">
					<img src="<?php echo $this->_asset('default/images/billboard/sample-1.jpg'); ?>" alt="">
				</div>
				<div class="subtitle">
					<div class="subtitle-container">
						Lorem ipsum dolor sit amet 1
					</div>
				</div>
			</div>
			<div class="item">
				<div class="image">
					<img src="<?php echo $this->_asset('default/images/billboard/sample-2.jpg'); ?>" alt="">
				</div>
				<div class="subtitle">
					<div class="subtitle-container">
						Lorem ipsum dolor sit amet 2
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-content" id="projetos">
	<div class="section-content-container">
		<header>
			<h2>Projetos</h2>
			<h3>Confira aqui os nossos principais projetos</h3>
		</header>

		<div class="grid grid-items-4">
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos'); ?>" class="block-project">
					<img src="<?php echo $this->_asset('default/images/projetos-edificios-residenciais.jpg'); ?>" alt="EdifÃ­cios residenciais">
					<span class="double-row">edifí­cios residenciais</span>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos'); ?>" class="block-project">
					<img src="<?php echo $this->_asset('default/images/projetos-residenciais.jpg'); ?>" alt="Residenciais">
					<span>residenciais</span>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos'); ?>" class="block-project">
					<img src="<?php echo $this->_asset('default/images/projetos-comerciais.jpg'); ?>" alt="Interiores">
					<span>comerciais</span>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos'); ?>" class="block-project">
					<img src="<?php echo $this->_asset('default/images/projetos-outros.jpg'); ?>" alt="Interiores">
					<span>outros</span>
				</a>
			</div>
		</div>
	</div>
</section>

<section class="section-content" id="empresa" style="background-image: url(<?php echo $this->_asset('default/images/empresa-bg.png') . '),url(' . $this->_asset('default/images/empresa-bg-custom.jpg'); ?>)">
	<div class="section-content-container">
		<header>
			<h2>Empresa</h2>
		</header>

		<div class="text">
			<p>A opção de ser arquiteto surgiu por influência do período de um ano em que residiu no exterior, em uma pequena cidade Northborough próxima a Boston, no estado  de Massachusetts (USA), com apenas 11.000 habitantes, lá participou de decisões  importantes junto com a comunidade para o planejamento da cidade, fazendo com  que visasse inicialmente a área de urbanismo.</p>
			<p>Em 1974 iniciou sua vida acadêmica na faculdade de Arquitetura e Urbanismo da Universidade Federal da Bahia (UFBA), e de imediato estagiou em escritórios de arquitetura acompanhando obras residenciais, logo após iniciou um estágio na Empreendimentos Odebrecht, em Salvador, sob a diretoria do Dr. Sérgio Gaudenzi onde permaneceu até a conclusão do curso em 1979. Após formado, passou o foco do urbanismo para a arquitetura de edificações por conseqüência e exigências do mercado.</p>
			<p>A graduação coincidiu com uma crise na construção civil. A Empreendimentos Odebrecht praticamente foi desativada, o que o fez procurar novas opções para seu desenvolvimento profissional. Surgiu uma oportunidade de trabalho no Departamento de Edificações do Estado de Sergipe (órgão também extinto), onde permaneceu durante 06 anos, para depois se dedicar à vida profissional como autônomo e então estruturar o escritório.</p>
			<p>Hoje Paulo Rehm mantém um escritório que leva seu nome e atua na área de projetos de edificações, urbanismo e consultoria. O escritório conta com profissionais que formam uma equipe harmônica e altamente qualificada, pronta para enfrentar novos desafios. Formada pelos arquitetos Rita Gouveia, Alexandre Carvalho, Mariana Gusmão, entre outros; e técnicos em edificações e estagiários, que juntos justificam – “a produção da arquitetura é um trabalho de equipe”. Atualmente o escritório mantém o foco voltado para a área de edificações residenciais, comerciais e urbanismo, tendo como parceiras as mais importantes construtoras e incorporadoras do mercado.</p>
			<p>Arquiteto baiano, radicado em Aracaju há mais de 30 anos, Paulo vem ao longo de sua trajetória profissional conquistando credibilidade e, acima de tudo, a cada projeto realizado, conquista um amigo.</p>
		</div>
	</div>
</section>

<section class="section-content" id="arquitetos">
	<div class="section-content-container">
		<header>
			<h2>Arquitetos</h2>
			<h3>Veja aqui os nossos arquitetos e saiba um pouco mais sobre eles</h3>
		</header>

		<div class="showcase grid grid-items-3">
			<div class="container">
				<div class="grid-item">
					<a href="#" class="block-profile" data-modal="<?php echo $this->_url('arquiteto'); ?>">
						<div class="image" style="background-image: url(<?php echo $this->_asset('default/images/niemeyer.jpg'); ?>);"></div>
						<h4>Paulo Rehm</h4>
						<p>Clique aqui e saiba um pouco mais</p>
					</a>
				</div>
				<div class="grid-item">
					<a href="#" class="block-profile" data-modal="<?php echo $this->_url('arquiteto'); ?>">
						<div class="image" style="background-image: url(<?php echo $this->_asset('default/images/niemeyer.jpg'); ?>);"></div>
						<h4>Paulo Rehm</h4>
						<p>Clique aqui e saiba um pouco mais</p>
					</a>
				</div>
				<div class="grid-item">
					<a href="#" class="block-profile" data-modal="<?php echo $this->_url('arquiteto'); ?>">
						<div class="image" style="background-image: url(<?php echo $this->_asset('default/images/niemeyer.jpg'); ?>);"></div>
						<h4>Paulo Rehm</h4>
						<p>Clique aqui e saiba um pouco mais</p>
					</a>
				</div>
				<div class="grid-item">
					<a href="#" class="block-profile" data-modal="<?php echo $this->_url('arquiteto'); ?>">
						<div class="image" style="background-image: url(<?php echo $this->_asset('default/images/niemeyer.jpg'); ?>);"></div>
						<h4>Paulo Rehm</h4>
						<p>Clique aqui e saiba um pouco mais</p>
					</a>
				</div>
				<div class="grid-item">
					<a href="#" class="block-profile" data-modal="<?php echo $this->_url('arquiteto'); ?>">
						<div class="image" style="background-image: url(<?php echo $this->_asset('default/images/niemeyer.jpg'); ?>);"></div>
						<h4>Paulo Rehm</h4>
						<p>Clique aqui e saiba um pouco mais</p>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-content" id="novidades">
	<div class="section-content-container">
		<header>
			<h2>Novidades</h2>
			<ul>
				<li><a href="<?php echo $this->_url('novidades'); ?>">Ver todas</a></li>
			</ul>
		</header>

		<div class="grid grid-items-3">
			<div class="grid-item">
				<a href="<?php echo $this->_url('novidades/viewer'); ?>" class="block-post">
					<img src="<?php echo $this->_asset('default/images/post.jpg'); ?>" alt="">
					<h4>Lorem ipsum dolor sit amet voluptas dicta corporis</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex nihil voluptatibus corrupti, blanditiis minima. Nesciunt, reiciendis iure, accusamus sit eaque obcaecati aliquam, consectetur magnam, atque quos quaerat quibusdam voluptas eum!</p>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('novidades/viewer'); ?>" class="block-post">
					<img src="<?php echo $this->_asset('default/images/post.jpg'); ?>" alt="">
					<h4>Lorem ipsum dolor sit amet voluptas dicta corporis</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex nihil voluptatibus corrupti, blanditiis minima. Nesciunt, reiciendis iure, accusamus sit eaque obcaecati aliquam, consectetur magnam, atque quos quaerat quibusdam voluptas eum!</p>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('novidades/viewer'); ?>" class="block-post">
					<img src="<?php echo $this->_asset('default/images/post.jpg'); ?>" alt="">
					<h4>Lorem ipsum dolor sit amet voluptas dicta corporis</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex nihil voluptatibus corrupti, blanditiis minima. Nesciunt, reiciendis iure, accusamus sit eaque obcaecati aliquam, consectetur magnam, atque quos quaerat quibusdam voluptas eum!</p>
				</a>
			</div>
		</div>
	</div>
</section>

<section class="section-content" id="clientes">
	<div class="section-content-container">
		<header>
			<h2>Clientes</h2>
		</header>

		<div class="grid grid-rows grid-collapse grid-items-7">
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/allianz.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/bbc.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/brasil-2014.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/brastemp.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/calvin-klein.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/coca-cola.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/volkswagen.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/allianz.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/bbc.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/brasil-2014.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/brastemp.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/calvin-klein.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/coca-cola.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/volkswagen.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/allianz.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/bbc.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/brasil-2014.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/brastemp.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/calvin-klein.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/coca-cola.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
			<div class="grid-item">
				<div class="block-grayscale-toggle">
					<img src="<?php echo $this->_asset('default/images/clients/volkswagen.png'); ?>" alt="" class="block-responsive">
				</div>
			</div>
		</div>
	</div>
</section>

