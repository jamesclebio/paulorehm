<section class="section-content" id="novidades-inside">
	<div class="section-content-container">
		<header>
			<h2>Novidades</h2>
			<ul>
				<li><a href="javascript:history.back();">Voltar</a></li>
				<li><a href="<?php echo $this->_url('novidades'); ?>">Ver todas</a></li>
			</ul>
		</header>

		<section class="text">
			<header>
				<h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit esse officiis <?php echo $this->getNameController(); ?></h1>
			</header>			

			<div class="clearfix">
				<div class="block-media-left">
					<a href="<?php echo $this->_asset('default/images/post.jpg'); ?>" class="lightbox"><img src="<?php echo $this->_asset('default/images/post.jpg'); ?>" alt=""></a>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis ea facere excepturi quaerat architecto facilis suscipit consectetur quae debitis. Laudantium culpa, hic veniam. Praesentium consectetur maiores quae asperiores quidem, aliquid.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum alias maxime nam mollitia, corporis dolorem fugit labore nulla atque, laudantium molestiae illo et placeat consequuntur veniam! Exercitationem accusantium possimus molestias.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse saepe perspiciatis earum a id fugit. Fugit alias, recusandae suscipit voluptatem nesciunt soluta fugiat commodi beatae ducimus fuga nam dolore quaerat minima perferendis tempora cum consequatur minus itaque saepe aut. Eveniet officiis velit, itaque fugiat quas culpa similique quibusdam nam in animi delectus numquam quo, cupiditate obcaecati totam possimus aliquid nulla.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio ut soluta inventore cum architecto quo error minima asperiores sit pariatur fuga, omnis, blanditiis corrupti nesciunt dolorem maiores labore, fugiat quaerat aspernatur unde nisi voluptatum dolor! Assumenda dolorem, suscipit eaque rem amet in consequuntur quas aperiam ullam? Reprehenderit placeat eius explicabo neque, at quo sunt voluptatibus aperiam quos ab atque quasi vero obcaecati corrupti culpa doloribus consectetur autem ipsam commodi, ratione voluptatum, eaque! Deserunt eos, quia. Odio quidem ullam sit laudantium porro veniam obcaecati quia enim magni, debitis ducimus ut temporibus necessitatibus deserunt iusto, deleniti, nam dolore perferendis soluta asperiores harum!</p>
			</div>
		</section>
	</div>
</section>

