<section class="section-content" id="projetos-inside">
	<nav class="nav-top">
		<ul>
			<li><a href="#">Residencial</a></li>
			<li><a href="#">Edifícios residenciais</a></li>
			<li><a href="#">Interiores</a></li>
		</ul>
	</nav>

	<div class="banner"></div>

	<!-- <div class="block-alert block-alert-empty">
		<p><strong>Ops! Sem conteúdo...</strong></p>
		<p>Desculpe, ainda não temos conteúdo para mostrar aqui.</p>
	</div> -->

	<div class="section-content-container">
		<div class="grid grid-rows grid-items-3">
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos/viewer'); ?>" class="block-project-2">
					<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="">
					<span>Lorem ipsum dolor sit amet voluptas dicta corporis</span>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos/viewer'); ?>" class="block-project-2">
					<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="">
					<span>Lorem ipsum dolor sit amet voluptas dicta corporis</span>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos/viewer'); ?>" class="block-project-2">
					<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="">
					<span>Lorem ipsum dolor sit amet voluptas dicta corporis</span>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos/viewer'); ?>" class="block-project-2">
					<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="">
					<span>Lorem ipsum dolor sit amet voluptas dicta corporis</span>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos/viewer'); ?>" class="block-project-2">
					<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="">
					<span>Lorem ipsum dolor sit amet voluptas dicta corporis</span>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos/viewer'); ?>" class="block-project-2">
					<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="">
					<span>Lorem ipsum dolor sit amet voluptas dicta corporis</span>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos/viewer'); ?>" class="block-project-2">
					<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="">
					<span>Lorem ipsum dolor sit amet voluptas dicta corporis</span>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos/viewer'); ?>" class="block-project-2">
					<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="">
					<span>Lorem ipsum dolor sit amet voluptas dicta corporis</span>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('projetos/viewer'); ?>" class="block-project-2">
					<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="">
					<span>Lorem ipsum dolor sit amet voluptas dicta corporis</span>
				</a>
			</div>
		</div>

		<div class="pagination">
			<ul>
				<li><a href="#"><span class="icon-arrow-left-b"></span></a></li>
				<li class="current"><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><span>...</span></li>
				<li><a href="#">6</a></li>
				<li><a href="#">7</a></li>
				<li><a href="#">8</a></li>
				<li><a href="#">9</a></li>
				<li><a href="#">10</a></li>
				<li><a href="#"><span class="icon-arrow-right-b"></span></a></li>
			</ul>
		</div>
	</div>
</section>

