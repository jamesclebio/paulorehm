<section class="section-content" id="novidades-inside">
	<div class="section-content-container">
		<header>
			<h2>Novidades</h2>
			<ul>
				<li><a href="javascript:history.back();">Voltar</a></li>
			</ul>
		</header>

		<div class="grid grid-rows grid-items-3">
			<div class="grid-item">
				<a href="<?php echo $this->_url('novidades/viewer'); ?>" class="block-post">
					<img src="<?php echo $this->_asset('default/images/post.jpg'); ?>" alt="">
					<h4>Lorem ipsum dolor sit amet voluptas dicta corporis</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex nihil voluptatibus corrupti, blanditiis minima. Nesciunt, reiciendis iure, accusamus sit eaque obcaecati aliquam, consectetur magnam, atque quos quaerat quibusdam voluptas eum!</p>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('novidades/viewer'); ?>" class="block-post">
					<img src="<?php echo $this->_asset('default/images/post.jpg'); ?>" alt="">
					<h4>Lorem ipsum dolor sit amet voluptas dicta corporis</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex nihil voluptatibus corrupti, blanditiis minima. Nesciunt, reiciendis iure, accusamus sit eaque obcaecati aliquam, consectetur magnam, atque quos quaerat quibusdam voluptas eum!</p>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('novidades/viewer'); ?>" class="block-post">
					<img src="<?php echo $this->_asset('default/images/post.jpg'); ?>" alt="">
					<h4>Lorem ipsum dolor sit amet voluptas dicta corporis</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex nihil voluptatibus corrupti, blanditiis minima. Nesciunt, reiciendis iure, accusamus sit eaque obcaecati aliquam, consectetur magnam, atque quos quaerat quibusdam voluptas eum!</p>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('novidades/viewer'); ?>" class="block-post">
					<img src="<?php echo $this->_asset('default/images/post.jpg'); ?>" alt="">
					<h4>Lorem ipsum dolor sit amet voluptas dicta corporis</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex nihil voluptatibus corrupti, blanditiis minima. Nesciunt, reiciendis iure, accusamus sit eaque obcaecati aliquam, consectetur magnam, atque quos quaerat quibusdam voluptas eum!</p>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('novidades/viewer'); ?>" class="block-post">
					<img src="<?php echo $this->_asset('default/images/post.jpg'); ?>" alt="">
					<h4>Lorem ipsum dolor sit amet voluptas dicta corporis</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex nihil voluptatibus corrupti, blanditiis minima. Nesciunt, reiciendis iure, accusamus sit eaque obcaecati aliquam, consectetur magnam, atque quos quaerat quibusdam voluptas eum!</p>
				</a>
			</div>
			<div class="grid-item">
				<a href="<?php echo $this->_url('novidades/viewer'); ?>" class="block-post">
					<img src="<?php echo $this->_asset('default/images/post.jpg'); ?>" alt="">
					<h4>Lorem ipsum dolor sit amet voluptas dicta corporis</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex nihil voluptatibus corrupti, blanditiis minima. Nesciunt, reiciendis iure, accusamus sit eaque obcaecati aliquam, consectetur magnam, atque quos quaerat quibusdam voluptas eum!</p>
				</a>
			</div>
		</div>

		<div class="pagination">
			<ul>
				<li><a href="#"><span class="icon-arrow-left-b"></span></a></li>
				<li class="current"><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><span>...</span></li>
				<li><a href="#">6</a></li>
				<li><a href="#">7</a></li>
				<li><a href="#">8</a></li>
				<li><a href="#">9</a></li>
				<li><a href="#">10</a></li>
				<li><a href="#"><span class="icon-arrow-right-b"></span></a></li>
			</ul>
		</div>
	</div>
</section>

