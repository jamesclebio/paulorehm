<?php
$nav = array(
	'home' => $this->_url('root') . '#!/home',
	'novidades' => $this->_url('root') . '#!/novidades',
	'empresa' => $this->_url('root') . '#!/empresa',
	'arquitetos' => $this->_url('root') . '#!/arquitetos',
	'projetos' => $this->_url('root') . '#!/projetos',
	'clientes' => $this->_url('root') . '#!/clientes',
	'contato' => $_SERVER['REQUEST_URI'] . '#!/contato'
);

// Novidades
if ($this->getNameController() === 'novidades') {
	if ($this->getNameAction()) {
		$nav['novidades'] = $this->_url('novidades');
	} else {
		$nav['novidades'] = $_SERVER['REQUEST_URI'] . '#!/novidades-inside';
	}
}
?>

<li><a href="<?php echo $nav['home']; ?>">Home</a></li>
<li><a href="<?php echo $nav['projetos']; ?>">Projetos</a></li>
<li><a href="<?php echo $nav['empresa']; ?>">Empresa</a></li>
<li><a href="<?php echo $nav['arquitetos']; ?>">Arquitetos</a></li>
<li><a href="<?php echo $nav['novidades'] ?>">Novidades</a></li>
<li><a href="<?php echo $nav['clientes']; ?>">Clientes</a></li>
<li><a href="<?php echo $nav['contato'] ?>">Contato</a></li>

