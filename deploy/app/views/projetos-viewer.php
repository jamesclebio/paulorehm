<section class="section-content" id="projetos-inside">
	<nav class="nav-top">
		<ul>
			<li><a href="#">Residencial</a></li>
			<li><a href="#">Edifícios residenciais</a></li>
			<li><a href="#">Interiores</a></li>
		</ul>
	</nav>

	<div class="banner"></div>

	<div class="section-content-container">
		<header>
			<ul>
				<li><a href="javascript:history.back();">Voltar</a></li>
				<li><a href="<?php echo $this->_url('projetos'); ?>">Ver todos</a></li>
			</ul>
		</header>

		<section class="text">
			<header>
				<h1>Lorem ipsum dolor sit amet</h1>
			</header>			

			<div class="clearfix">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quis ea facere excepturi quaerat architecto facilis suscipit consectetur quae debitis. Laudantium culpa, hic veniam. Praesentium consectetur maiores quae asperiores quidem, aliquid.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse saepe perspiciatis earum a id fugit. Fugit alias, recusandae suscipit voluptatem nesciunt soluta fugiat commodi beatae ducimus fuga nam dolore quaerat minima perferendis tempora cum consequatur minus itaque saepe aut. Eveniet officiis velit, itaque fugiat quas culpa similique quibusdam nam in animi delectus numquam quo, cupiditate obcaecati totam possimus aliquid nulla.</p>
			</div>
		</section>

		<div class="separate">
			<div class="block-embed">
				<iframe src="http://www.youtube.com/embed/28h3SmnJZZI" frameborder="0" width="560" height="315"></iframe>
			</div>
		</div>

		<div class="separate">
			<div class="grid grid-rows grid-items-3">
				<div class="grid-item">
					<a href="<?php echo $this->_asset('default/images/project.jpg'); ?>" rel="gallery" class="lightbox">
						<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="" class="block-responsive">
					</a>
				</div>
				<div class="grid-item">
					<a href="<?php echo $this->_asset('default/images/project.jpg'); ?>" rel="gallery" class="lightbox">
						<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="" class="block-responsive">
					</a>
				</div>
				<div class="grid-item">
					<a href="<?php echo $this->_asset('default/images/project.jpg'); ?>" rel="gallery" class="lightbox">
						<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="" class="block-responsive">
					</a>
				</div>
				<div class="grid-item">
					<a href="<?php echo $this->_asset('default/images/project.jpg'); ?>" rel="gallery" class="lightbox">
						<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="" class="block-responsive">
					</a>
				</div>
				<div class="grid-item">
					<a href="<?php echo $this->_asset('default/images/project.jpg'); ?>" rel="gallery" class="lightbox">
						<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="" class="block-responsive">
					</a>
				</div>
				<div class="grid-item">
					<a href="<?php echo $this->_asset('default/images/project.jpg'); ?>" rel="gallery" class="lightbox">
						<img src="<?php echo $this->_asset('default/images/project.jpg'); ?>" alt="" class="block-responsive">
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

