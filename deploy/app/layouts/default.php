<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> no-js" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $this->getTitle(); ?></title>
	<meta name="description" content="<?php echo $this->getDescription(); ?>">
	<link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
	<script src="<?php echo $this->_asset('default/scripts/head.js'); ?>"></script>
	<?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
	<?php $this->getAnalytics(); ?>
	<?php $this->getBodyPrepend(); ?>

	<header class="global-header">
		<div class="global-header-container">
			<h1><a href="<?php echo $this->_url('root'); ?>#!/home">Paulo Rehm Arquitetos</a></h1>

			<nav>
				<ul>
					<?php include 'nav-items.php'; ?>
				</ul>
			</nav>
		</div>
	</header>

	<div class="global-content">
		<?php $this->getView(); ?>

		<section class="section-content" id="contato">
			<div class="section-content-container">
				<header>
					<h2>Contato</h2>
					<h3>Para maiores informações, entre em contato conosco</h3>
				</header>

				<?php $this->getAlert(); ?>

				<div class="grid grid-items-2">
					<div class="grid-item">
						<address>
							Rua Duque de Caxias, 285<br>
							São José | CEP 49015-320<br>
							Tel. <a href="tel:07932116817">+55 79 3211 6817</a><br>
							Aracaju | Sergipe | Brasil
						</address>

						<ul class="social">
							<li><a href="#" target="_blank" title="Facebook"><span class="icon-social-facebook"></span></a></li>
							<li><a href="#" target="_blank" title="Instagram"><span class="icon-social-instagram"></span></a></li>
						</ul>
					</div>
					<div class="grid-item">
						<form id="form-contact" method="post" action="<?php echo $this->_url('contato/mensagem'); ?>" class="form">
							<input name="url_return" type="hidden" value="<?php echo $nav['contato'] ?>">

							<fieldset>
								<legend>Contato</legend>
								<div class="grid grid-items-2">
									<div class="grid-item">
										<label><input name="name" type="text" placeholder="Seu nome *" required></label>
									</div>
									<div class="grid-item">
										<label><input name="phone" type="text" placeholder="Seu telefone" class="mask-phone"></label>
									</div>
								</div>
								<label><input name="email" type="email" placeholder="Seu email *" required></label>
								<label><textarea name="message" cols="30" rows="10" maxlength="600" class="height-100" placeholder="Sua mensagem *" data-field-count required></textarea></label>

								<div class="block-action">
									<button type="submit" class="button button-large button-warning">Enviar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>

	<footer class="global-footer">
		<div class="global-footer-container">
			<a href="http://www.agw.com.br" target="_blank" class="agw">AGW Internet</a>
		</div>
	</footer>

	<script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
	<?php $this->getBodyAppend(); ?>
</body>
</html>