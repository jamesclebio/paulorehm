<?php
class Projetos extends Page
{
	public function index() {
		$this->setLayout('default');
		$this->setView('projetos');
		$this->setTitle('Projetos - Paulo Rehm Arquitetos');
		$this->setDescription('');
		$this->setAnalytics(true);
		$this->setSession(true);
	}

	public function viewer() {
		$this->setView('projetos-viewer');
	}
}

