<?php
class Contato extends Page
{
	public function index() {
		$this->setLayout(false);
		$this->setView(false);
		$this->setTitle('');
		$this->setDescription('');
		$this->setAnalytics(false);
		$this->setSession(true);
	}

	public function mensagem() {
		if ($_POST) {
			$mail = new MailHelper();
			$mail->setHeader(
				'From: ' . $_POST['name'] . ' <' . $_POST['email'] . '>' . "\r\n" .
				'Reply-To: ' . $_POST['email'] . "\r\n"
			);
			$mail->setTo('dev@jamesclebio.com.br');
			$mail->setSubject('[Paulo Rehm Arquitetos - Contato] - ' . $_POST['name']);
			$mail->setBody(
				'Nome: ' . $_POST['name'] . "\n" .
				'E-mail: ' . $_POST['email'] . "\n" .
				'Telefone: ' . $_POST['phone'] . "\n\n" .
				'---' . "\n" .
				'Mensagem:' . "\n\n" .
				$_POST['message'] . "\n\n" .
				'*** Enviado a partir de ' . $_SERVER['HTTP_HOST'] . ' em ' . date('d/m/Y H:i:s')
			);
			$mail->setAlertSuccess('<p><strong>Sua mensagem foi enviada com sucesso!</strong></p><p>Daremos um retorno o mais breve possível.</p>');
			$mail->send($this, $_POST['url_return'], $_POST['url_return']);
		}
	}
}

