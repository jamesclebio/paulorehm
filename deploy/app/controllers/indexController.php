<?php
class Index extends Page
{
	public function index() {
		$this->setLayout('default');
		$this->setView('index');
		$this->setTitle('Paulo Rehm Arquitetos');
		$this->setDescription('');
		$this->setAnalytics(true);
		$this->setSession(true);
	}
}

