<?php
class Novidades extends Page
{
	public function index() {
		$this->setLayout('default');
		$this->setView('novidades');
		$this->setTitle('Novidades - Paulo Rehm Arquitetos');
		$this->setDescription('');
		$this->setAnalytics(true);
		$this->setSession(true);
	}

	public function viewer() {
		$this->setView('novidades-viewer');
	}
}

