;(function($, window, document, undefined) {
	'use strict';

	window.showcase = {
		settings: {
			autoinit: true
		},

		wrapper: '.showcase',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				template_nav = '<div class="nav"><a href="#" class="prev"></a><a href="#" class="next"></a></div>';

			$wrapper.append(template_nav);

			$wrapper.find('.container').carouFredSel({
				responsive: true,
				items: {
					visible: 3,
					minimum: 4
				},
				scroll: 1,
				auto: {
					timeoutDuration: 4000
				},
				prev: $wrapper.find('.nav .prev'),
				next: $wrapper.find('.nav .next')
			});
		},

		init: function() {
			var	$wrapper = $(this.wrapper);

			if ($wrapper.length) {
				this.build();
			}
		}
	};
}(jQuery, this, this.document));

