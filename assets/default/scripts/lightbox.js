;(function($, window, document, undefined) {
	'use strict';

	window.lightbox = {
		wrapper: '.lightbox',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.colorbox();
		},

		init: function() {
			var	$wrapper = $(this.wrapper);

			if ($wrapper.length) {
				this.build();
			}
		}
	};
}(jQuery, this, this.document));

