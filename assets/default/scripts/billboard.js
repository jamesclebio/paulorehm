;(function($, window, document, undefined) {
	'use strict';

	window.billboard = {
		wrapper: '.billboard',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				$container = $wrapper.find('.container'),
				template_nav = '<div class="nav"><a href="#" class="prev"></a><a href="#" class="next"></a></div>',
				init = new Image();

			// Basis goHash scroll bug prevent)
			init.src = $wrapper.find('.item:first-child img').attr('src');

			init.onload = function() {
				$wrapper.show().append(template_nav);

				$container.carouFredSel({
					responsive: true,
					items: {
						height: 'variable',
						visible: 1,
						minimum: 2
					},
					scroll: {
						fx: 'crossfade',
						pauseOnHover: true
					},
					auto: {
						timeoutDuration: 4000
					},
					prev: $wrapper.find('.nav .prev'),
					next: $wrapper.find('.nav .next')
				});

				basis.goHash.init();
			};
		},

		init: function() {
			var	$wrapper = $(this.wrapper);

			if ($wrapper.length) {
				this.build();
			} else {
				basis.goHash.init();
			}
		}
	};
}(jQuery, this, this.document));

