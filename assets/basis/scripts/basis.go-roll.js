;(function($, window, document, undefined) {
	'use strict';

	basis.goRoll = function(target) {
		var	$parent = $('body, html'),
			$target = $(target);

		if (!$target.length) {
			$target = $parent;
		}

		$parent.animate({
			scrollTop: $target.offset().top - 105
		});
	};
}(jQuery, this, this.document));

