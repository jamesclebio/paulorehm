;(function($, window, document, undefined) {
	'use strict';

	basis.goTop = {
		settings: {
			autoinit: true
		},

		wrapper: '.go-top',

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.bindings();
			}
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.on({
				click: function(e) {
					basis.goRoll();
					e.preventDefault();
				}
			});
		}
	};
}(jQuery, this, this.document));

