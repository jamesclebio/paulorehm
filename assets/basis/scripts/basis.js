window.basis = {
	name: 'basis',
	version: '{{ VERSION }}',
	author: 'James Clébio (jamesclebio@gmail.com)',

	init: function() {
		var resources = [];

		if (arguments.length) {
			resources = arguments;
		} else {
			for (var i in this) {
				if (i !== 'init' && typeof(this[i].init) === 'function') {
					resources.push(i);
				}
			}
		}

		for (var j in resources) {
			if (this[resources[j]].settings.autoinit) {
				this[resources[j]].init();
			}
		}
	}
};

