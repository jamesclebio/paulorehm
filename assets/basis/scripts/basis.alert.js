;(function($, window, document, undefined) {
	'use strict';

	basis.alert = {
		settings: {
			autoinit: true
		},

		wrapper: '.alert',
		template_close: '<a href="#" class="close icon-close-circled"></a>',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.each(function() {
				if ($(this).data('alertClose')) {
					$(this).prepend(that.template_close);
				}
			});
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$(document).on({
				click: function(e) {
					$(this).closest(that.wrapper).fadeOut(100, function() {
						$(this).remove();
					});

					e.preventDefault();
				}
			}, that.wrapper + ' .close');
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
			}

			that.bindings();
		}
	};
}(jQuery, this, this.document));

