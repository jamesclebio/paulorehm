;(function($, window, document, undefined) {
	'use strict';

	basis.fieldExtend = {
		settings: {
			autoinit: true
		},

		wrapper: '[data-field-extend]',
		parent_list: [],

		build: function($parent, update) {
			var	that = this,
				$wrapper = $(that.wrapper);

			if (!update) {
				$wrapper.each(function() {
					var	parent = '[name="' + $(this).attr('name') + '"]';

					if (that.parent_list.indexOf(parent) == -1)
						that.parent_list.push(parent);
				});
			}

			if (!$parent) {
				$(that.parent_list.join(',')).filter(that.wrapper).each(function() {
					that.toggle($(this));
				});
			} else {
				$parent.filter(that.wrapper).each(function() {
					that.toggle($(this));
				});
			}
		},

		toggle: function($this) {
			var	$target = $($this.data('fieldExtend')),
				$target_input = $target.find(':input');

			if ($this.is(':checked')) {
				$target.show();
				$target_input.removeAttr('disabled');
			} else {
				$target.hide();
				$target_input.attr('disabled', 'disabled');
			}
		},

		bindings: function() {
			var	that = this;

			$(that.parent_list.join(',')).on({
				change: function() {
					var $parent = $('[name="' + $(this).attr('name') + '"]');

					that.build($parent, true);
				}
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
				that.bindings();
			}
		}
	};
}(jQuery, this, this.document));

