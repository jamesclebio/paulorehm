;(function($, window, document, undefined) {
	'use strict';

	basis.goHash = {
		settings: {
			autoinit: false
		},

		wrapper: 'a[href*="#!/"]',

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
				that.bindings();
			}
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.on({
				click: function(e) {
					var	target = $(this).attr('href').replace(/.+#!\//, '#');

					basis.goRoll(target);
				}
			});
		},

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				target = (/#!\//.test(window.location.hash)) ? window.location.hash.replace('!/', '') : false;

			if (!/\/$/.test(window.location.pathname)) {
				window.location.pathname += '/';
			}

			if (target) {
				basis.goRoll(target);
			}
		}
	};
}(jQuery, this, this.document));

