;(function($, window, document, undefined) {
	'use strict';

	basis.toggleRoll = {
		settings: {
			autoinit: true
		},

		wrapper: '[data-toggle-roll]',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.each(function() {
				that.binding($(this));
			});
		},

		binding: function($this) {
			var	data = $this.data('toggleRoll'),
				offset = /\d+\|/.test(data) ? data.replace(/(\d+)\|.+/, '$1') : 0,
				class_toggle = data.replace(/\d+\|/, '');

			$(window).scroll(function() {
				if ($(window).scrollTop() >= offset) {
					$this.addClass(class_toggle);
				} else {
					$this.removeClass(class_toggle);
				}
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
			}
		}
	};
}(jQuery, this, this.document));

